using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlappyBird
{
    public class PipePool : MonoBehaviour
    {

        [SerializeField] private GameObject pipePref;
        [SerializeField] private GameObject greenpipe;
        [SerializeField] private GameObject nightpipe;
        [SerializeField] private List<GameObject> pipeList;
        [SerializeField]private int numberOfPipes = 5;
        [SerializeField] GameObject nightMap;

        private static PipePool instance;

    public static PipePool Instance { get { return instance; } }

        private void Awake()
        {
            if(instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        void Start()
        {
            if (!nightMap.activeSelf)
            {
                pipePref = greenpipe;
            }
            else
            {
                pipePref = nightpipe;
            }
            AddPipesToPool(numberOfPipes);

        }
        private void AddPipesToPool(int amount)
        {
            for (int i = 0; i < numberOfPipes; i++)
            {
                GameObject pipe = Instantiate(pipePref);
                pipe.SetActive(false);
                pipeList.Add(pipe);
                pipe.transform.parent = transform;
            }
        }
        public GameObject RequestPipe()
        {
            for(int i = 0; i < pipeList.Count; i++)
            {
                if (!pipeList[i].activeSelf)
                {
                    pipeList[i].SetActive(true);
                    return pipeList[i];
                }
            }
            AddPipesToPool(1);
            pipeList[pipeList.Count - 1].SetActive(true);
            return pipeList[pipeList.Count - 1];
        }
        
    }
}
