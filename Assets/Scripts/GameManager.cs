using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace FlappyBird
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private float timeToReloadScene;

        [Space, SerializeField]
        private UnityEvent onGameOver;
        [SerializeField]
        private UnityEvent onIncreaseScore;

        [SerializeField] GameObject nightMap;
        [SerializeField] GameObject redBird;
        [SerializeField] GameObject yellowBird;
        [SerializeField] GameObject blueBird;

        public int ramdom;


        public bool isGameOver { get; private set; }
        public int scoreCount { get; private set; }

        public static GameManager Instance
        {
            get; private set;
        }

        private void Awake()
        {
            if (Instance != null)
                DestroyImmediate(gameObject);
            else
                Instance = this;

            ramdom = Random.Range(0, 15);
            Debug.Log("ramdom = " + ramdom);
            if (ramdom < 7)
            {
                nightMap.SetActive(false);
            }
            if (ramdom >= 7)
            {
                nightMap.SetActive(true);
            }

            
            if(ramdom <= 5)
            {
                yellowBird.SetActive(true);
                redBird.SetActive(false);
                blueBird.SetActive(false);
            }
            else if(ramdom <= 10)
            {
                yellowBird.SetActive(false);
                redBird.SetActive(true);
                blueBird.SetActive(false);
            }
            else if (ramdom >= 11)
            {
                yellowBird.SetActive(false);
                redBird.SetActive(false);
                blueBird.SetActive(true);
            }
        }

        public void GameOver()
        {
            Debug.Log("GameManager :: GameOver()");

            isGameOver = true;

            if (onGameOver != null)
                onGameOver.Invoke();



            StartCoroutine(ReloadScene());
        }

        public void IncreaseScore()
        {
            scoreCount++;

            if (onIncreaseScore != null)
                onIncreaseScore.Invoke();
        }
        private IEnumerator ReloadScene()
        {
            yield return new WaitForSeconds(timeToReloadScene);

            

            SceneManager.LoadScene(0);
        }
    }
}